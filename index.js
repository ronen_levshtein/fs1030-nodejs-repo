import dotenv from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import routeUser from './src/routeUsers.js';
import routeAll from './src/routeAll.js';
import routePatient from './src/routePatientDetail.js';
import routeProvider from './src/routeProviderDetail.js';


const app = express();
dotenv.config();
const port = process.env.PORT || 3005;
// console.log(`Your port is: ${port}`);

app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use("/users", routeUser);
app.use("/api", routeAll);
app.use("/patient", routePatient);
app.use("/provider", routeProvider)

app.use((err, req, res, next) => {
    if (req.headersSent) {
        return next(err)
    }
    console.log("local error");
    res.status(404).send(`{"message": "not found"}`);
});

app.use('*', (req, res, next) => {
    console.log("global error");
    res.status(404).send(`{"message": "not found"}`);
});


app.listen(port, () => { console.log(`Listnening on port ${port}`); });

export default app;