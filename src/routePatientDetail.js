import express from 'express';
const bodyParser = require('body-parser');
import cors from 'cors';
import jwt from 'jsonwebtoken'
import bcrypt, { hash } from 'bcrypt'
import { v4 as uuidv4 } from "uuid";
import { dbQuery, dbQry, existsUser, getLoginByEmail } from './util/dbHandler';

const app = express();
const router = express.Router();
app.use(cors());
app.use(express.json());

app.use(bodyParser.urlencoded({extended: true}))

router.get("/getpatients", (req, res) => {
    const sqlSelect = "SELECT * FROM patient_detail";
    dbQuery(sqlSelect, (err, result) => {
        res.send(result);
    });
});

router.get("/getpatient/:id", (req, res) => {
    const id = req.params.id

    const sqlSelectPatient = "SELECT * FROM patient_detail WHERE id = ?";
    dbQuery(sqlSelectPatient, id, (err, result) => {
        res.send(result);
    });
});

router.delete("/deletepatient/:id", (req, res) => {
    const id = req.params.id

    const sqlDelete = "DELETE FROM patient_detail WHERE id = ?";
    dbQuery(sqlDelete, id, (err, result) => {
        if(err) console.log(err);    
        res.send(result);
    });
})

router.post("/insertinto", (req, res,) => {

    const name = req.body.name;
    const email = req.body.email;
    const address = req.body.address;
    const city = req.body.city;
    const province = req.body.province;
    const postal_code = req.body.postal_code;
    const health_card_number = req.body.health_card_number;
    const blood_type = req.body.blood_type;
    const DOB = req.body.DOB;
    const Gender = req.body.Gender;
    // const permission = req.body.permission;


    const sqlInsert = "INSERT INTO patient_detail (name, email, address, city, province, postal_code, health_card_number, blood_type, DOB, Gender) VALUES (?,?,?,?,?,?,?,?,?,?)";
    let params = [name, email, address, city, province, postal_code, health_card_number, blood_type, DOB, Gender];
    dbQuery(sqlInsert, params, (err, result) => {
        res.send(result);
    })
});


/* Route registers new users and adds it to the database 
Returns an error for incomplete data */
router.put("/update/:id", (req, res, next) => {


});



export default router;