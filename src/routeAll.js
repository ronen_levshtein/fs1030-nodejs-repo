import express from 'express';
import { db } from './util/connection.js';


const router = express.Router();

// All Patients GET API
router.get("/patients", function (req, res) {
    db.query(
        "SELECT * FROM PATIENT_DETAIL order by id",
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// Patient by id GET API
router.get("/patient/:patientid", function (req, res) {
    db.query(
        `SELECT * FROM PATIENT_DETAIL WHERE id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// Patient Details GET API
router.get("/patientdetails/:patientid", function (req, res) {
    db.query(
        `SELECT a.name as patientname, a.email, a.address, a.city, a.province, a.postal_code, a.health_card_number, a.blood_type, a.DOB, a.Gender, b.notes, b.patient_id, b.employee_id, b.date_time, c.physical_exams, c.medicines_taken, c.allergies, c.immunization_status, c.surgeries, c.patient_id,d.date_time, d.status, d.bill_details, d.amount as billamount, d.patient_id,e.amount as paidamount, e.method, e.date_time, e.patient_id, e.bill_number,f.date_time, f.reason as visitreason, f.diagnosis, f.bill_number, f.patient_id,g.name as testname, g.results, g.Radiology_Image, g.patient_id  FROM PATIENT_DETAIL A, NOTES B, MEDICAL_HISTORY C, BILL D, PAYMENT E, VISIT F, LAB_TESTS G WHERE A.id=B.patient_id AND A.id=C.patient_id AND A.id=D.patient_id AND A.id=E.patient_id AND A.id=F.patient_id AND A.id=G.patient_id AND A.id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


router.get("/notes/:patientid", function (req, res) {
    db.query(
        `SELECT * FROM NOTES WHERE patient_id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


router.get("/medical_history/:patientid", function (req, res) {
    db.query(
        `SELECT * FROM MEDICAL_HISTORY WHERE patient_id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


router.get("/bill/:patientid", function (req, res) {
    db.query(
        `SELECT * FROM BILL WHERE patient_id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


router.get("/payment/:patientid", function (req, res) {
    db.query(
        `SELECT * FROM PAYMENT WHERE patient_id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


router.get("/visit/:patientid", function (req, res) {
    db.query(
        `SELECT * FROM VISIT WHERE patient_id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


router.get("/lab_tests/:patientid", function (req, res) {
    db.query(
        `SELECT * FROM LAB_TESTS WHERE patient_id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// All Providers GET API
router.get("/providers", function (req, res) {
    db.query(
        "SELECT * FROM EMPLOYEE order by id",
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// Provider by id GET API
router.get("/provider/:id", function (req, res) {
    db.query(
        `SELECT * FROM EMPLOYEE WHERE id=${req.params.id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// Patient POST API
router.post("/patient", function (req, res) {
    db.query(
        "INSERT INTO PATIENT_DETAIL (name, email, address, city, province, postal_code, health_card_number, blood_type, DOB, Gender) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
        [req.body.name, req.body.email, req.body.address, req.body.city, req.body.province, req.body.postal_code, req.body.health_card_number, req.body.blood_type, req.body.DOB, req.body.Gender],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

// Patient Details POST API
router.post("/notes", function (req, res) {
    db.query(
        "INSERT INTO NOTES (notes, patient_id, employee_id, date_time) VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP())",
        [req.body.id, req.body.notes, req.body.patient_id, req.body.employee_id],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

router.post("/medical_history", function (req, res) {
    db.query(
        "INSERT INTO MEDICAL_HISTORY (physical_exams, medicines_taken, allergies, immunization_status, surgeries, patient_id) VALUES(?, ?, ?, ?, ?, ?)",
        [req.body.physicalexams, req.body.medicinestaken, req.body.allergies, req.body.immunizationstatus, req.body.surgeries, req.body.patientid],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

router.post("/bill", function (req, res) {
    db.query(
        "INSERT INTO BILL (date_time, status, bill_details, amount, patient_id) VALUES(CURRENT_TIMESTAMP(), ?, ?, ?, ?)",
        [req.body.billstatus, req.body.billdetails, req.body.amount, req.body.patientid],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

router.post("/payment", function (req, res) {
    db.query(
        "INSERT INTO PAYMENT (amount, method, date_time, patient_id, bill_number) VALUES(?, ?, CURRENT_TIMESTAMP(), ?, ?)",
        [req.body.paidamount, req.body.paidmethod, req.body.patientid, req.body.billnumber],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
    db.query(
        `UPDATE BILL SET receipt_id = (SELECT receipt_id FROM PAYMENT WHERE bill_number=${req.params.billnumber}) AND patient_id=${req.params.patientid}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

router.post("/visit", function (req, res) {
    db.query(
        "INSERT INTO VISIT (date_time, reason, diagnosis, bill_number, patient_id) VALUES(CURRENT_TIMESTAMP(), ?, ?, ?, ?)",
        [req.body.visitreason, req.body.diagnosis, req.body.billnumber, req.body.patientid],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

router.post("/lab_tests", function (req, res) {
    db.query(
        "INSERT INTO LAB_TESTS (name, results, Radiology_Image, patient_id) VALUES(?, ?, ?, ?)",
        [req.body.testname, req.body.testresults, req.body.RadiologyImage, req.body.patientid],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

// Patient Details POST API
router.post("/patientdetails", function (req, res) {
    db.query(
        `
      INSERT INTO NOTES (notes, patient_id, employee_id, date_time) VALUES(?, ?, ?, CURRENT_TIMESTAMP());
      INSERT INTO MEDICAL_HISTORY (physical_exams, medicines_taken, allergies, immunization_status, surgeries, patient_id) VALUES(?, ?, ?, ?, ?, ?);
      INSERT INTO BILL (date_time, status, bill_details, amount, patient_id) VALUES(CURRENT_TIMESTAMP(), ?, ?, ?, ?);
      INSERT INTO PAYMENT (amount, method, date_time, patient_id, bill_number) VALUES(?, ?, CURRENT_TIMESTAMP(), ?, ?);
      UPDATE BILL SET receipt_id = (SELECT receipt_id FROM PAYMENT WHERE bill_number=${req.params.billnumber}) AND patient_id=${req.params.patientid};
      INSERT INTO VISIT (date_time, reason, diagnosis, bill_number, patient_id) VALUES(CURRENT_TIMESTAMP(), ?, ?, ?, ?);
      INSERT INTO LAB_TESTS (name, results, Radiology_Image, patient_id) VALUES(?, ?, ?, ?)`,
        [req.body.notes, req.body.patient_id, req.body.employee_id, req.body.physicalexams, req.body.medicinestaken, req.body.allergies, req.body.immunizationstatus, req.body.surgeries, req.body.patientid, req.body.billstatus, req.body.billdetails, req.body.amount, req.body.patientid, req.body.paidamount, req.body.paidmethod, req.body.patientid, req.body.billnumber, req.body.visitreason, req.body.diagnosis, req.body.billnumber, req.body.patientid, req.body.testname, req.body.testresults, req.body.RadiologyImage, req.body.patientid],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

// Provider POST API
router.post("/provider", function (req, res) {
    db.query(
        "INSERT INTO EMPLOYEE (name, email, phone, status, start_date, end_date, designation) VALUES(?, ?, ?, ?, ?, ?, ?)",
        [req.body.name, req.body.email, req.body.phone, req.body.status, req.body.startdate, req.body.enddate, req.body.designation],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

// Patient PUT API
router.put("/patient/:id", (req, res) => {
    const { name, email, address, city, province, postalcode, healthcardnumber, bloodtype, DOB, Gender } = req.body;
    db.query(
        `UPDATE PATIENT_DETAIL SET name="${name}", email="${email}", address="${address}", city="${city}", province="${province}", postal_code="${postalcode}", health_card_number="${healthcardnumber}", blood_type="${bloodtype}", DOB="${DOB}", Gender="${Gender}"  WHERE id=${req.params.id};`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// // Patient Details PUT API - DONOT USE IT
// router.put("/patientdetails", (req, res) => {
//   const { notes, employeeid, physicalexams, medicinestaken, immunizationstatus, surgeries, billstatus, billdetails, billamount, paidamount, paidmethod, billnumber, visitreason, diagnosis, testname, testresults, RadiologyImage} = req.body;
//   db.query(
//     `
//     UPDATE NOTES SET notes="${notes}", employee_id="${employeeid}", date_time=CURRENT_TIMESTAMP()  WHERE id=${req.params.id} AND patient_id=${req.params.patientid};
//     UPDATE MEDICAL_HISTORY SET physical_exams="${physicalexams}", medicines_taken="${medicinestaken}", immunization_status="${immunizationstatus}", surgeries="${surgeries}"  WHERE id=${req.params.id} AND patient_id=${req.params.patientid};
//     UPDATE BILL SET date_time=CURRENT_TIMESTAMP(), status="${billstatus}", bill_details="${billdetails}", amount="${billamount}"  WHERE bill_number=${req.params.billnumber} AND patient_id=${req.params.patientid};
//     UPDATE PAYMENT SET amount="${paidamount}", method="${paidmethod}", date_time=CURRENT_TIMESTAMP(), bill_number="${billnumber}"  WHERE receipt_id=${req.params.receiptid} AND patient_id=${req.params.patientid};
//     UPDATE VISIT SET date_time=CURRENT_TIMESTAMP(), reason="${visitreason}", diagnosis="${diagnosis}", bill_number="${billnumber}"  WHERE id=${req.params.id} AND patient_id=${req.params.patientid};
//     UPDATE LAB_TESTS SET name="${testname}" results="${testresults}", Radiology_Image="${RadiologyImage}"  WHERE WHERE id=${req.params.id} AND patient_id=${req.params.patientid}`,
//     function (error, results, fields) {
//       if (error) throw error;
//       return res.status(200).send(results);
//     }
//   );
// });

router.put("/notes", (req, res) => {
    const { id, notes, patient_id, employee_id, date_time } = req.body;
    db.query(
        `UPDATE NOTES SET notes="${notes}", employee_id="${employee_id}", date_time=CURRENT_TIMESTAMP()  WHERE id=${id} AND patient_id=${patient_id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

router.put("/medical_history", (req, res) => {
    const { id, physical_exams, medicines_taken, allergies, immunization_status, surgeries, patient_id } = req.body;
    db.query(
        `UPDATE MEDICAL_HISTORY SET physical_exams="${physical_exams}", medicines_taken="${medicines_taken}", immunization_status="${immunization_status}", surgeries="${surgeries}"  WHERE id=${id} AND patient_id=${patient_id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

router.put("/bill", (req, res) => {
    const { bill_number, date_time, status, bill_details, amount, patient_id, receipt_id } = req.body;
    db.query(
        `UPDATE BILL SET date_time=CURRENT_TIMESTAMP(), status="${status}", bill_details="${bill_details}", amount="${amount}"  WHERE bill_number=${bill_number} AND patient_id=${patient_id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

router.put("/payment", (req, res) => {
    const { receipt_id, amount, method, date_time, patient_id, bill_number } = req.body;
    db.query(
        `UPDATE PAYMENT SET amount="${amount}", method="${method}", date_time=CURRENT_TIMESTAMP(), bill_number="${bill_number}"  WHERE receipt_id=${id} AND patient_id=${patient_id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

router.put("/visit", (req, res) => {
    const { id, date_time, reason, diagnosis, bill_number, patient_id } = req.body;
    db.query(
        `UPDATE VISIT SET date_time=CURRENT_TIMESTAMP(), reason="${reason}", diagnosis="${diagnosis}", bill_number="${bill_number}"  WHERE id=${id} AND patient_id=${patient_id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

router.put("/lab_tests", (req, res) => {
    const { id, name, results, Radiology_Image, patient_id } = req.body;
    db.query(
        `UPDATE LAB_TESTS SET name="${name}" results="${results}", Radiology_Image="${Radiology_Image}"  WHERE id=${id} AND patient_id=${patient_id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// Provider PUT API
router.put("/provider/:id", (req, res) => {
    const { name, email, phone, status, startdate, enddate, designation } = req.body;
    db.query(
        `UPDATE EMPLOYEE SET name="${name}", email="${email}", phone="${phone}", status="${status}", start_date="${startdate}", end_date="${enddate}", designation="${designation}"  WHERE id=${req.params.id};`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// Patient DELETE API
router.delete("/patient/:id", (req, res) => {
    db.query(
        `
      DELETE FROM PATIENT_DETAIL WHERE id=${req.params.id};
      DELETE FROM NOTES WHERE patient_id=${req.params.id};
      DELETE FROM MEDICAL_HISTORY WHERE patient_id=${req.params.id};
      DELETE FROM BILL WHERE patient_id=${req.params.id};
      DELETE FROM PAYMENT WHERE patient_id=${req.params.id};
      DELETE FROM VISIT WHERE patient_id=${req.params.id};
      DELETE FROM LAB_TESTS WHERE patient_id=${req.params.id};`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// Provider DELETE API
router.delete("/provider/:id", (req, res) => {
    db.query(
        `DELETE FROM EMPLOYEE WHERE id=${req.params.id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


export default router;