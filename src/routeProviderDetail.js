import express from 'express';
import jwt from 'jsonwebtoken'
import bcrypt, { hash } from 'bcrypt'
import { v4 as uuidv4 } from "uuid";

const router = express.Router();


/* Get all the provider details */
router.get("/list", (req, res, next) => {


});


/* Get the provider details by id*/
router.get("/list/:id", (req, res, next) => {


});


/* Get the provider details by search criteria data passed */
router.get("/list/search", (req, res, next) => {


});

/* Route registers new providers 
Returns error for invalid data*/

router.post("/create", (req, res, next) => {


});


/* Route updates provider details */
router.put("/update/:id", (req, res, next) => {


});

/* Route deletes an existing provider*/
router.delete("/delete/:id", (req, res, next) => {


});

export default router;