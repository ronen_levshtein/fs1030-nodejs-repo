import mysql from 'mysql';
import dotenv, { config } from 'dotenv';


dotenv.config();
const dbHost = 'localhost';
const dbUser = 'nodeclient';
const dbPassword = '123456';
const dbName = 'fs1030_emr';

const db = mysql.createConnection({
    host: `${dbHost}`,
    user: `${dbUser}`,
    password: `${dbPassword}`,
    database: `${dbName}`,
    // multipleStatements: true
});

db.connect(function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log("MySQL database is connected");
    }
});

export { db };
