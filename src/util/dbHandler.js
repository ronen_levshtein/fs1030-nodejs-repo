import dotenv from 'dotenv'
import util from 'util';
import { promises as fs } from 'fs';
import path from 'path';
import { db as EMRDB } from './connection.js';


dotenv.config();

function dbQuery(databaseQuery, params) {
    return new Promise(data => {
        EMRDB.query(databaseQuery, params, function (error, result) {
            // change db->connection for your code
            if (error) {
                console.log(error);
                throw error;
            }

            //console.log(result);
            try {
                console.log("In db query", result);

                data(result);

            } catch (error) {
                data({});
                throw error;
            }

        });
    });

}

function dbQry(databaseQuery) {
    return new Promise(data => {
        EMRDB.query(databaseQuery, function (error, result) {
            // change db->connection for your code
            if (error) {
                console.log(error);
                throw error;
            }

            //console.log(result);
            try {
                console.log("In db query", result);

                data(result);

            } catch (error) {
                data({});
                throw error;
            }

        });
    });

}


async function getUsers() {
    let sql = `SELECT * FROM USERS`;
    let users = [];

    users = await dbQuery(sql).then(function (results) { return results }).catch(function (err) { throw err; });

    return users;
}

async function getUserById(id) {
    let sql = `SELECT * FROM USERS WHERE id=?`;
    let user = [];
    let params = id;

    user = await dbQuery(sql, params).then(function (results) { return results }).catch(function (err) { throw err; });

    return user;
}

async function getLoginByEmail(email) {
    // let sql = `SELECT * FROM LOGIN WHERE email=?`;
    let sql = `SELECT * FROM EMPLOYEE JOIN LOGIN ON EMPLOYEE.id = LOGIN.EMPLOYEE_ID WHERE EMAIL=?`;
    let user = [];
    let params = email;
    console.log(`emil :"${params}"`)
    user = await dbQuery(sql, params).then(function (results) {
        console.log(results);
        return results;
    }).catch(function (err) { throw err; });
    console.log(`users : ${user[0]}`)
    return user;
}

async function getPatients() {
    let sql = `SELECT * FROM PATIENT_DETAIL`;
    let patients = [];

    patients = await dbQuery(sql).then(function (results) { return results; }).catch(function (err) { throw err; });

    return patients;
}

async function existsUser(email) {
    let sql = `SELECT * FROM EMPLOYEE JOIN LOGIN ON EMPLOYEE.id = LOGIN.EMPLOYEE_ID WHERE EMAIL=?`;
    let params = email;
    let users = [];
    let exist = false;

    users = await dbQuery(sql, params).then(function (results) { return results; }).catch(function (err) { throw err; });
    console.log(users);


    if (users == undefined) {
        exist = false;
        //return exist;
    }
    if (users.length > 0) {
        exist = true;
    }
    console.log(exist);
    return exist;
}

export { getPatients, dbQuery, dbQry, getLoginByEmail, existsUser };